"use strict";

var HtmlWebpackplugin = require("html-webpack-plugin");
var path = require("path");

const { join, resolve } = require("path");

module.exports = {
  mode: "development",
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js"
  },
  devtool: "inline-source-map",
  devServer: {
    contentBase: "./dist"
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        use: "babel-loader"
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|jpe?g|svg|gif)$/i,
        loader: "file-loader",
        options: {
          name: "[name].[ext]",
          outputPath: "assets/images"
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackplugin({
      template: join(__dirname, "public", "index.html")
    })
  ]
};
